use Test;
use HTTP::UserAgent :simple;
use Test::IO::Capture;

plan 7;

if %*ENV<NETWORK_TESTING> {
    my $url = 'http://perlmonkeys.org/';

    my $get;

    todo 'possibly host is down';
    lives-ok { $get = get $url }, 'get works';

    if $get {
        is $get.substr($get.chars - 9), "\n</html>\n", 'get 1/1';

        my $code;

        prints-stdout-ok { $code = getprint $url }, $get, 'getprint 1/2';

        is $code, 200, 'getprint 2/2';

        getstore $url, 'newfile';

        is slurp('newfile'), $get, 'getstore 1/1';

        unlink 'newfile';

        throws-like "use HTTP::UserAgent :simple; get('http://perlmonkeys.org/404here')", X::HTTP::Response, message => "Response error: '404 Not Found'";
    }
    else {
        skip 'get is failed', 4;
    }

    my $head;

    todo 'possibly host is down';
    lives-ok { $head = head $url }, 'head works';

    if $head {
        is $head.elems, 5, 'got the right number of elements';
    }
    else {
        skip 'head is failed';
    }
}
else {
    skip-rest "NETWORK_TESTING not set won't do network tests";
}
