# HTTP::UserAgent

Web user agent class for [Raku](https://raku.org) (formerly known as Perl 6).

## Usage

```Perl
use HTTP::UserAgent;

my $ua = HTTP::UserAgent.new;
$ua.timeout = 10;

my $response = $ua.get("URL");

if $response.is-success {
    say $response.content;
} else {
    die $response.status-line;
}
```

## Installation

To install it using [Zef](https://github.com/ugexe/zef) (a module management tool bundled with [Rakudo Star](https://rakudo.org/)):

```
$ zef install HTTP::UserAgent
```

## Testing

To run tests:

```
$ prove6 -Ilib ./t
```

## Documentation

Please see the documentation links listed below:

- [HTTP::Cookies](https://gitlab.com/pheix/http-useragent/-/blob/main/lib/HTTP/Cookies.rakumod#L112)
    - [HTTP::Cookie](https://gitlab.com/pheix/http-useragent/-/blob/main/lib/HTTP/Cookie.rakumod#L17)
- [HTTP::Header](https://gitlab.com/pheix/http-useragent/-/blob/main/lib/HTTP/Header.rakumod#L109)
    - [HTTP::Header::Field](https://gitlab.com/pheix/http-useragent/-/blob/main/lib/HTTP/Header/Field.rakumod#L12)
- [HTTP::Message](https://gitlab.com/pheix/http-useragent/-/blob/main/lib/HTTP/Message.rakumod#L97)
- [HTTP::Request](https://gitlab.com/pheix/http-useragent/-/blob/main/lib/HTTP/Request.rakumod#L79)
- [HTTP::Response](https://gitlab.com/pheix/http-useragent/-/blob/main/lib/HTTP/Response.rakumod#L35)
- [HTTP::UserAgent](https://gitlab.com/pheix/http-useragent/-/blob/main/lib/HTTP/UserAgent.pm6#L597)
    - [HTTP::UserAgent::Common](https://gitlab.com/pheix/http-useragent/-/blob/main/lib/HTTP/UserAgent/Common.rakumod#L20)

## To-do List and Future Ideas

~~strikethrough text~~ means **done**.

- clean up
- speed up

### HTTP::UserAgent
- ~~HTTP Auth~~
- let user set his own cookie jar
- ~~make getprint() return the code response~~
- ~~security fix - use File::Temp to create temporary cookie jar~~
- use Promises
- ~~make SSL dependency as optional~~

### HTTP::Cookies
- path restriction

### OpenSSL
- ~~fix NativeCall's int bug~~
- make it work on more platforms

### IO::Socket::SSL
- make it work on more platforms
- make SSL support more reliable
- add throwing exception on failing SSL
- more tests

### Credits

[https://github.com/sergot/http-useragent](https://github.com/sergot/http-useragent)


### Maintainer

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
